//
//  AppDelegate.h
//  ADaoEngineTestApp-iOS
//
//  Created by Craig Zheng on 4/11/16.
//  Copyright © 2016 cz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

