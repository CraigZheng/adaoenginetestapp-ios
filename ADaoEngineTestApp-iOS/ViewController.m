//
//  ViewController.m
//  ADaoEngineTestApp-iOS
//
//  Created by Craig Zheng on 4/11/16.
//  Copyright © 2016 cz. All rights reserved.
//

#import "ViewController.h"

@import ADaoEngine;

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self downloadContents];
    [self postContents];
}

- (void)downloadContents {
    // Download latest forums.
    [ADaoDownloader updateForumsWithCompletion:^(BOOL success, NSArray<ADaoForumGroup *> * _Nullable forums, NSError * _Nullable error) {
        
    }];
    // Download threads for a forum.
    [ADaoDownloader downloadThreadsFor:4
                                    at:1
                            completion:^(BOOL success, NSArray<ADaoThread *> * _Nullable threads, NSError * _Nullable error) {
        
    }];
    // Download replies for a thread.
    [ADaoDownloader downloadRepliesFor:10088110
                                    at:3
                            completion:^(BOOL success, ADaoThread * _Nullable targetThread, NSArray<ADaoThread *> * _Nullable replies, NSError * _Nullable error) {
                                
                            }];
    [ADaoCookieEater updateCookiesWithCompletion:^(BOOL success, NSArray<NSHTTPCookie *> * _Nullable cookies, NSError * _Nullable error) {
        
    }];
}

- (void)postContents {
    // Comment out to test posting.
    return;
    
    ADaoThread *postThread = [ADaoThread new];
    postThread.title = postThread.content = postThread.email = postThread.name = @"肥贼测试中";
    
    ADaoPostImage *postImage = [ADaoPostImage new];
    // UIImage data, must be either jpg or gif.
    postImage.imageData = [NSData dataWithContentsOfURL: [[NSBundle mainBundle] URLForResource:@"running_ac"
                                                                                 withExtension:@"gif"]];
    // If its gif, must specify.
    postImage.imageFormat = @"gif";
    
    [ADaoPostMaker postNewWithThread:postThread
                                  to:20
                                with:postImage
                          completion:^(BOOL success, NSDictionary<id<NSCopying>,NSObject *> * _Nullable responseHeaders, NSString * _Nullable responseHtml, NSError * _Nullable error) {
                              
                          }];
    [ADaoPostMaker replyWith:postThread
                          to:10088110
                        with:postImage
                  completion:^(BOOL success, NSDictionary<id<NSCopying>,NSObject *> * _Nullable responseHeaders, NSString * _Nullable responseHtml, NSError * _Nullable error) {
                      
                  }];
}

- (void)configuration {
    [[ADaoConfiguration sharedInstance] setUserAgent:@"Name of your user agent"];
    // Can also be used to set an array of NSHTTPCookie obejcts.
    [[ADaoConfiguration sharedInstance] setCookies:nil];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
