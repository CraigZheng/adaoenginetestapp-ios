//
//  main.m
//  ADaoEngineTestApp-iOS
//
//  Created by Craig Zheng on 4/11/16.
//  Copyright © 2016 cz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
